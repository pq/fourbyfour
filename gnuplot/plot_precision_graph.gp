#!/usr/bin/env -S gnuplot -c

# SPDX-FileCopyrightText: 2021 Pekka Paalanen <pq@iki.fi>
#
# SPDX-License-Identifier: MIT

# inspired by http://www.gnuplotting.org/attractive-plots/

reset

if (GPVAL_VERSION < 5.0) exit error 'Gnuplot >= 5.0 required'

if (ARG1 eq '') {
	print 'This script needs one file name argument for the data file.'
	print 'Output will be written to a file of the same name with .svg suffix added.'
	exit error 'no input data file given'
}

set terminal svg size 580,350 fixed enhanced rounded dashed background '#ffffff'
set output ARG1.'.svg'

# yellow af9416
# red af1616
# green 36af16
# blue 1645af
# cyan 16aeaf
# magenta ae16af
# purple 7616af

set style line 1 lc rgb '#af1616' lt 1 lw 2 dt 2 # red
set style line 2 lc rgb '#36af16' lt 1 lw 2 dt 1 # green
set style line 3 lc rgb '#1645af' lt 1 lw 2 dt 3 # blue
set style line 4 lc rgb '#7616af' lt 1 lw 2 dt 4 # purple
set style line 5 lc rgb '#000000' lt 1 lw 2 dt 1 # black

set style line 11 lc rgb '#808080' lt 1
set border 1+2+8 back ls 11
set tics out nomirror
set xtics 1, 3
set mxtics 3
set y2tics out nomirror

set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12

set key center bmargin reverse Left maxrows 3

set xlabel 'log_2 (random matrix condition number)'
set ylabel 'observation count'
set y2label 'bits'
set xrange [] noextend

plot ARG1 using 1:2 title 'ok (count)' with histeps ls 2, \
	'' using 1:3 title 'non-invertible (count)' with histeps ls 3, \
	'' using 1:4 title 'bad result (count)' with histeps ls 1, \
	'' using 1:5 axes x1y2 title 'min precision (bits)' with lines ls 4, \
	5/log10(2) axes x1y2 title '5 digits precision (bits)' with lines ls 5
