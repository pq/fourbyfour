#!/bin/bash

# SPDX-FileCopyrightText: 2021 Pekka Paalanen <pq@iki.fi>
#
# SPDX-License-Identifier: MIT

set -o xtrace -o errexit

pip3 install --user git+https://github.com/mesonbuild/meson.git@0.56
pip3 install --user git+https://github.com/fsfe/reuse-tool@v0.12.1
