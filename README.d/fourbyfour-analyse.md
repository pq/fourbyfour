---
SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
SPDX-License-Identifier: CC-BY-4.0
---

[Front page](../README.md)

# Command `fourbyfour-analyse`

This tool mainly computes the 2-norm condition number of a 4x4 matrix
and makes some statements based on it.

Instructions:
```
$ ./build/src/fourbyfour-analyse
Analyse a 4-by-4 matrix for inversion precision.

Usage: ./build/src/fourbyfour-analyse a11 a12 a13 a14 a21 a22 a23 a24 a31 a32 a33 a34 a41 a42 a43 a44
where the arguments a11..a44 form a 4-by-4 matrix in row-major order:
	a11 a12 a13 a14
	a21 a22 a23 a24
	a31 a32 a33 a34
	a41 a42 a43 a44
Only argument a11 is mandatory. Arguments must be given in the above order,
you cannot skip some matrix elements from between. You can leave out any number
of trailing arguments though, in that case the respective elements are taken
from the identity matrix.
Each argument a11..a44 must be convertible to a double precision floating
point value without extra characters at the end.
The analysis is printed to the standard output.
```

Example:
```
$ ./build/src/fourbyfour-analyse 1 0 0 1920
Your input matrix A is
	           1            0            0         1920
	           0            1            0            0
	           0            0            1            0
	           0            0            0            1

The singular values of A are: 1920, 1, 1, 0.000520833
The condition number according to 2-norm of A is 3.686e+06.

This means that if you were to solve the linear system Ax=b for vector x,
in the worst case you would lose 6.6 digits (21.8 bits) of precision.
The condition number is how much errors in vector b would be amplified
when solving x even with infinite computational precision.

Compare this to the precision of vectors b and x:

- Single precision floating point has 7.2 digits (24 bits) of precision,
  leaving your result with no correct digits.
  Single precision, matrix A has rank 3 which means that the solution space
  for x has 1 dimension and therefore has many solutions.

- Double precision floating point has 16.0 digits (53 bits) of precision,
  leaving your result with 9.4 correct digits (31 correct bits).
  Double precision, matrix A has full rank which means the solution x is
  unique.

NOTE! The above gives you only an upper limit on errors.
If the upper limit is low, you can be confident of your computations. But,
if the upper limit is high, it does not necessarily imply that your
computations will be doomed.
```
