---
SPDX-FileCopyrightText: 2022 Collabora, Ltd.
SPDX-License-Identifier: CC-BY-4.0
---

[Front page](../README.md)

# Command `fourbyfour-generate`

This tool generates pseudo-random 4x4 matrices as explained in
[Generating Near-Singular Matrices](../README.d/precision_testing.md#generating-near-singular-matrices).

Generating good matrices for testing your own matrix inversion routines
is complicated. This tool is intended to help you by generating the
matrices for you, according to your parameters.

Help:
```
$ ./build/src/fourbyfour-generate -h
Generate a pseudo-random 4x4 matrix with given parameters

Usage: ./build/src/fourbyfour-generate [options]
Options may include:
  -b, --bits B     Set matrix 2-norm condition number to 2**B
  -c, --cond C     Set matrix 2-norm condition number to C.
  -d, --det D      Set matrix absolute determinant value to D.
  -h, --help       This help text.
  -n, --count N    Number of matrices to generate.

The generated matrices are printed one per line. Row-major vs. column-major
ordering is insignificant, it doesn't affect the matrix properties.
If none of B, C nor D is given, the elements will be in range [-1.0, 1.0].
```

Example:
```
$ ./build/src/fourbyfour-generate -b 14 -d 150
-2.41666357216129732421e+01 -1.09068101093495073428e+01 6.52712632617247408007e+01 -4.98782673442838202504e+01 4.39410015792789039324e+01 3.50261565995370338644e-01 -7.53689846924090716129e+01 4.70829740012542856675e+01 1.58899589385709646194e+02 1.85861381121613788991e+01 -3.11596712205335620638e+02 2.11372430223174944786e+02 4.25902001798706493219e+01 1.14556253594314636501e+01 -9.74790146789948579453e+01 6.98692034497415193073e+01
```

Note that pseudo-random number generator is always initialized with the
same constant. Re-running the generator always uses the same random
matrices as the base before applying modifications to produce the
desired condition number and/or absolute determinant value.
