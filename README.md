---
SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
SPDX-License-Identifier: CC-BY-4.0
---

Documentation

- [`fourbyfour-analyse` tool](README.d/fourbyfour-analyse.md)
- [`fourbyfour-bench` tool](README.d/fourbyfour-bench.md)
- [`fourbyfour-generate` tool](README.d/fourbyfour-generate.md)
- [On Mathematical Notation](README.d/mathematical_notation.md)
- [Precision Testing Method](README.d/precision_testing.md)
- [Benchmark Results](README.d/results.md)

# Four by four

This project is an experiment attempting to analyze the correctness and speed
of 4x4 matrix inversions in different implementations.

The main feature of the fourbyfour static library is generating random
matrices with the desired condition number and/or determinant value.
The test suite ensures the basics work correctly.

Three programs are included:

- [`fourbyfour-analyse` tool](README.d/fourbyfour-analyse.md) computes a
  condition number for a given 4x4 matrix and explains what it means.

- [`fourbyfour-bench` tool](README.d/fourbyfour-bench.md) does speed and
  precision testing of various 4x4 matrix inversion implementations.
  The results can be plotted in a graph.
  The supported implementations are:
  - [Graphene](http://ebassi.github.io/graphene/)
  - [Weston](https://gitlab.freedesktop.org/wayland/weston) matrix

- [`fourbyfour-generate` tool](README.d/fourbyfour-generate.md) generates
  matrices that you can use to test your matrix inversion routines in
  Continuous Integration by copying the generated matrices into your
  test program, for example.

This project is licensed under [MIT](LICENSES/MIT.txt) for code and code
documentation, and [CC-BY-4.0](LICENSES/CC-BY-4.0.txt) for images and other
documents. See also [CONTRIBUTING](CONTRIBUTING.md).

This project depends on BLAS and LAPACK for a linear algebra implementation
that is known correct. As BLAS and LAPACK have various different
implementations and different distributions ship things differently, it's
possible the build does not find these correctly. The following have been
tested to work out of the box:

- Debian Buster: `libblas-dev`, `liblapacke-dev`
- Fedora 33: `blas-devel`, `lapack-devel`

The build is standard Meson:
```
$ meson build -Ddebug=false -Doptimization=3 -Dc_args=-march=native
$ meson test -C build
```

Hosted at [freedesktop.org Gitlab](https://gitlab.freedesktop.org/pq/fourbyfour).
Browse [the latest test suite code coverage results](https://gitlab.freedesktop.org/pq/fourbyfour/-/jobs/artifacts/master/file/coverage/index.html?job=coverage-preview).
