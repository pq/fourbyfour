/*
 * SPDX-FileCopyrightText: 2021 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <fourbyfour.h>
#include <graphene.h>

#include "fourbyfour-bench.h"

static void
fbfb_graphene_matrix_to_fbf(struct fbf_matrix *out,
			    const union fbfb_matrix *in)
{
	const graphene_matrix_t *mat = &in->graphene;
	int row, col;

	/*
	 * Transpose the matrix.
	 * See fbfb_graphene_transform() for why.
	 */

	/* Make this trivial to see it's correct, not fast. */
	for (row = 0; row < 4; row ++) {
		for (col = 0; col < 4; col++) {
			*fbf_matrix_elem(out, row, col) =
				graphene_matrix_get_value(mat, col, row);
		}
	}
}

static void
fbfb_graphene_matrix_from_fbf(union fbfb_matrix *out,
			      const struct fbf_matrix *in)
{
	float row_major[4 * 4];
	int row, col;

	/*
	 * Transpose the matrix.
	 * See fbfb_graphene_transform() for why.
	 */

	/* Make this trivial to see it's correct, not fast. */
	for (row = 0; row < 4; row ++) {
		for (col = 0; col < 4; col++) {
			row_major[row * 4 + col] =
				fbf_matrix_elem_get(in, col, row);
		}
	}

	graphene_matrix_init_from_float(&out->graphene, row_major);
}

static void
fbfb_graphene_vector_to_fbf(struct fbf_vector *out,
			    const union fbfb_vector *in)
{
	/*
	 * Transpose row vector into column vector.
	 * Graphene vectors are row vectors.
	 */
	out->v[0] = graphene_vec4_get_x(&in->graphene);
	out->v[1] = graphene_vec4_get_y(&in->graphene);
	out->v[2] = graphene_vec4_get_z(&in->graphene);
	out->v[3] = graphene_vec4_get_w(&in->graphene);
}

static void
fbfb_graphene_vector_from_fbf(union fbfb_vector *out,
			      const struct fbf_vector *in)
{
	/*
	 * Transpose column vector into row vector.
	 * Graphene vectors are row vectors.
	 */
	graphene_vec4_init(&out->graphene,
			   in->v[0], in->v[1], in->v[2], in->v[3]);
}

static enum fbfb_inversion_result
fbfb_graphene_invert(union fbfb_matrix *inv, const union fbfb_matrix *mat)
{
	/* Compute the inverse of an already transposed matrix. */
	if (graphene_matrix_inverse(&mat->graphene, &inv->graphene))
		return FBFB_INVERSION_RESULT_OK;
	else
		return FBFB_INVERSION_RESULT_NOT_INVERTIBLE;
}

static void
fbfb_graphene_transform(union fbfb_vector *yT,
			const union fbfb_matrix *AT,
			const union fbfb_vector *xT)
{
	/*
	 * By interface contract, we are supposed to compute
	 *       y = Ax.
	 *
	 * However, what we actually have as arguments here are
	 * y^T, A^T, and x^T, because our matrix and vector conversion
	 * functions also transpose.
	 *
	 * y^T and x^T are row vectors, so we are actually going to
	 * compute
	 *     y^T = x^T A^T.
	 *
	 * This is what we wanted, because taking transpose on both sides:
	 * (y^T)^T = (x^T A^T)^T
	 *       y = (A^T)^T (x^T)^T
	 *       y = Ax.
	 */
	graphene_matrix_transform_vec4(&AT->graphene, &xT->graphene,
				       &yT->graphene);
}

static const struct fbfb_interface implementation_graphene = {
	.name = "graphene",
	.desc = "a math library for 2D and 3D graphics",
	.precision = FBF_PRECISION_SINGLE,
	.matrix_to_fbf = fbfb_graphene_matrix_to_fbf,
	.matrix_from_fbf = fbfb_graphene_matrix_from_fbf,
	.vector_to_fbf = fbfb_graphene_vector_to_fbf,
	.vector_from_fbf = fbfb_graphene_vector_from_fbf,
	.invert = fbfb_graphene_invert,
	.transform = fbfb_graphene_transform,
	.decompose = NULL,
	.solve = NULL,
};

const struct fbfb_interface *
fbfb_interface_get_graphene(void)
{
	return &implementation_graphene;
}
