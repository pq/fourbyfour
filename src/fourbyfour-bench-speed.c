/*
 * SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#include <fourbyfour.h>

#include "fourbyfour-bench.h"

struct measurement {
	double secs;
	long long count;
};

struct measuring_setup {
	const struct fbfb_interface *impl;
	union fbfb_vector vec;

	const union fbfb_matrix *mats;
	const union fbfb_decomposition *decoms;
	int count;

	double seconds;
};

static bool alarm_rang;

static void
sig_alarm_handler(int s)
{
	if (s == SIGALRM)
		alarm_rang = true;
}

static void
set_alarm_handler(void)
{
	struct sigaction alrm;

	alrm.sa_handler = sig_alarm_handler;
	sigemptyset(&alrm.sa_mask);
	alrm.sa_flags = 0;
	sigaction(SIGALRM, &alrm, NULL);
}

static void
schedule_alarm(double seconds)
{
	struct itimerval it = {
		.it_interval = {
			.tv_sec = 0,
			.tv_usec = 0
		},
		.it_value = {
			.tv_sec = floor(seconds),
			.tv_usec = floor(1e6 * (seconds - floor(seconds)))
		}
	};

	if (setitimer(ITIMER_REAL, &it, NULL) < 0) {
		perror("setting up timer");
		exit(1);
	}
}

static void
stopwatch_reset(struct timespec *sw)
{
	clock_gettime(CLOCK_MONOTONIC, sw);
}

static double
stopwatch_read(const struct timespec *sw)
{
	struct timespec now = { 0 };

	clock_gettime(CLOCK_MONOTONIC, &now);

	return (double)(now.tv_sec - sw->tv_sec) +
	       1e-9 * (now.tv_nsec - sw->tv_nsec);
}

static union fbfb_matrix *
create_input_data(int N, const struct fbfb_interface *impl)
{
	union fbfb_matrix *mats;
	struct fbf_matrix R;
	int i;

	mats = calloc(N, sizeof *mats);
	if (!mats) {
		fprintf(stderr, "Error: out of memory.\n");
		exit(1);
	}

	for (i = 0; i < N; i++) {
		fbf_matrix_rand(&R, FBF_MATRIX_RAND_FLAG_COND, 1000.0, 0);
		impl->matrix_from_fbf(&mats[i], &R);
	}

	return mats;
}

static struct measurement
measure_explicit_inversion(const struct measuring_setup *setup)
{
	const struct fbfb_interface *impl = setup->impl;
	int count = setup->count;
	const union fbfb_matrix *mats = setup->mats;
	struct measurement meas = { 0 };
	union fbfb_matrix result;
	struct timespec sw;
	int i = 0;

	stopwatch_reset(&sw);
	while (!alarm_rang) {
		impl->invert(&result, &mats[i]);
		meas.count++;
		if (++i >= count)
			i = 0;
	}
	meas.secs = stopwatch_read(&sw);

	return meas;
}

static struct measurement
measure_vector_transform(const struct measuring_setup *setup)
{
	const struct fbfb_interface *impl = setup->impl;
	int count = setup->count;
	const union fbfb_matrix *mats = setup->mats;
	const union fbfb_vector *vec = &setup->vec;
	struct measurement meas = { 0 };
	union fbfb_vector result;
	struct timespec sw;
	int i = 0;

	stopwatch_reset(&sw);
	while (!alarm_rang) {
		impl->transform(&result, &mats[i], vec);
		meas.count++;
		if (++i >= count)
			i = 0;
	}
	meas.secs = stopwatch_read(&sw);

	return meas;
}

static struct measurement
measure_decompose(const struct measuring_setup *setup)
{
	const struct fbfb_interface *impl = setup->impl;
	int count = setup->count;
	const union fbfb_matrix *mats = setup->mats;
	struct measurement meas = { 0 };
	union fbfb_decomposition result;
	struct timespec sw;
	int i = 0;

	stopwatch_reset(&sw);
	while (!alarm_rang) {
		impl->decompose(&result, &mats[i]);
		meas.count++;
		if (++i >= count)
			i = 0;
	}
	meas.secs = stopwatch_read(&sw);

	return meas;
}

static struct measurement
measure_solve(const struct measuring_setup *setup)
{
	const struct fbfb_interface *impl = setup->impl;
	int count = setup->count;
	const union fbfb_vector *vec = &setup->vec;
	const union fbfb_decomposition *decoms = setup->decoms;
	struct measurement meas = { 0 };
	union fbfb_vector result;
	struct timespec sw;
	int i = 0;

	stopwatch_reset(&sw);
	while (!alarm_rang) {
		impl->solve(&result, &decoms[i], vec);
		meas.count++;
		if (++i >= count)
			i = 0;
	}
	meas.secs = stopwatch_read(&sw);

	return meas;
}

static double
measure(struct measuring_setup *setup,
	struct measurement (*func)(const struct measuring_setup *setup),
	const char *what,
	const char *funcname)
{
	struct measurement meas;
	struct fbf_value_units valun;
	double secs;

	printf("Measuring %s for %g seconds...\n", what, setup->seconds);

	alarm_rang = false;
	schedule_alarm(setup->seconds);
	meas = func(setup);

	secs = meas.secs / meas.count;
	fbf_metric_prefix(&valun, secs);
	printf("   done in %.3f seconds, %lld iterations, average %.3g %ss per %s call.\n",
	       meas.secs, meas.count, valun.value, valun.units, funcname);

	return secs;
}

int
fbfb_operation_speed(const struct fbfb_options *opts)
{
	struct measuring_setup setup;
	union fbfb_matrix *mats;
	union fbfb_decomposition *decoms;
	struct fbf_value_units valun[3];
	double explicit_inv_secs;
	double matrix_vector_secs;
	double decompose_secs = 0.0;
	double solve_secs = 0.0;
	double warmup_secs;
	int i;

	set_alarm_handler();

	if (opts->quick_test) {
		warmup_secs = 0.05;
		setup.seconds = 0.05;
	} else {
		warmup_secs = 1.0;
		setup.seconds = 5.0;
	}

	setup.count = 1000;
	setup.impl = opts->impl;

	setup.impl->vector_from_fbf(&setup.vec,
			&(struct fbf_vector){{ 1.7, -3.4, 2.2, 8.0 }});

	mats = create_input_data(setup.count, setup.impl);
	decoms = calloc(setup.count, sizeof *decoms);
	if (setup.impl->solve) {
		for (i = 0; i < setup.count; i++)
			setup.impl->decompose(&decoms[i], &mats[i]);
	}
	setup.mats = mats;
	setup.decoms = decoms;

	printf("Warming up for %g seconds...\n", warmup_secs);
	alarm_rang = false;
	schedule_alarm(warmup_secs);
	measure_explicit_inversion(&setup);

	explicit_inv_secs = measure(&setup, measure_explicit_inversion,
				    "explicit matrix inversion", "invert()");

	matrix_vector_secs = measure(&setup, measure_vector_transform,
				     "matrix-vector multiplication", "transform()");

	if (setup.impl->solve) {
		decompose_secs = measure(&setup, measure_decompose,
					 "decomposition for inversion",
					 "decompose()");

		solve_secs = measure(&setup, measure_solve,
				     "inverse-transformation solver",
				     "solve()");
	}

	printf("\n");
	printf("Speed test results for %s:\n", setup.impl->name);
	printf("\n");
	printf("            preparing   transforming          sum\n");
	printf("          the inverse       a vector\n");

	fbf_metric_prefix(&valun[0], explicit_inv_secs);
	fbf_metric_prefix(&valun[1], matrix_vector_secs);
	fbf_metric_prefix(&valun[2], explicit_inv_secs + matrix_vector_secs);
	printf("explicit %9.3g %ss   %9.3g %ss %9.3g %ss\n",
	       valun[0].value, valun[0].units,
	       valun[1].value, valun[1].units,
	       valun[2].value, valun[2].units);

	fbf_metric_prefix(&valun[0], decompose_secs);
	fbf_metric_prefix(&valun[1], solve_secs);
	fbf_metric_prefix(&valun[2], decompose_secs + solve_secs);
	if (setup.impl->solve) {
		printf("solver   %9.3g %ss   %9.3g %ss %9.3g %ss\n",
		       valun[0].value, valun[0].units,
		       valun[1].value, valun[1].units,
		       valun[2].value, valun[2].units);
	}

	free(decoms);
	free(mats);

	return 0;
}
