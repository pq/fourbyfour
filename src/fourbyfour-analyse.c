/*
 * SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#include <fourbyfour.h>

static bool
str2double(double *d_out, const char *str)
{
	double d;
	char *end;
	const char *errstr;
	int len = strlen(str);

	errno = 0;
	d = strtod(str, &end);
	if (errno == 0 && str + len == end) {
		*d_out = d;
		return true;
	}

	errstr = strerror(errno);
	if (str + len != end)
		errstr = "unexpected trailing characters";

	fprintf(stderr,
		"Error: could not interpret '%s' as a number: %s\n",
		str, errstr);
	return false;
}

static void
print_help(const char *prog)
{
	printf(
		"Analyse a 4-by-4 matrix for inversion precision.\n"
		"\n"
		"Usage: %s a11 a12 a13 a14 a21 a22 a23 a24 a31 a32 a33 a34 a41 a42 a43 a44\n"
		"where the arguments a11..a44 form a 4-by-4 matrix in row-major order:\n"
		"\ta11 a12 a13 a14\n"
		"\ta21 a22 a23 a24\n"
		"\ta31 a32 a33 a34\n"
		"\ta41 a42 a43 a44\n"
		"Only argument a11 is mandatory. Arguments must be given in the above order,\n"
		"you cannot skip some matrix elements from between. You can leave out any number\n"
		"of trailing arguments though, in that case the respective elements are taken\n"
		"from the identity matrix.\n"
		"Each argument a11..a44 must be convertible to a double precision floating\n"
		"point value without extra characters at the end.\n"
		"The analysis is printed to the standard output.\n",
		prog);
}

struct prec_desc {
	const char *name;
	double bits_significand;
	enum fbf_precision prec;
};

static const struct prec_desc precisions[] = {
	{ "Single", 24, FBF_PRECISION_SINGLE },
	{ "Double", 53, FBF_PRECISION_DOUBLE },
};

int
main(int argc, char *argv[])
{
	struct fbf_matrix mat;
	int r, c, i;
	struct fbf_matrix_conditions mc;

	fbf_matrix_identity(&mat);

	if (argc < 2 ||
	    strcmp(argv[1], "-h") == 0 ||
	    strcmp(argv[1], "--help") == 0) {
		print_help(argv[0]);
		return 0;
	}

	if (argc > 17) {
		fprintf(stderr, "Error: too many command line arguments.\n");
		print_help(argv[0]);
		return 1;
	}

	i = 1;
	for (r = 0; r < 4; r++) {
		for (c = 0; c < 4; c++) {
			if (!str2double(fbf_matrix_elem(&mat, r, c), argv[i]))
				return 1;

			if (++i >= argc)
				goto args_done;
		}
	}
args_done:

	printf("Your input matrix A is\n");
	fbf_matrix_print(stdout, &mat);

	fbf_matrix_cond_analyse(&mc, &mat, FBF_PRECISION_DOUBLE);
	printf("\n");
	printf("The singular values of A are: %g, %g, %g, %g\n",
	       mc.sigma.v[0], mc.sigma.v[1], mc.sigma.v[2], mc.sigma.v[3]);
	printf("The condition number according to 2-norm of A is %.4g.\n",
	       mc.sigma.v[0] / mc.sigma.v[3]);
	printf("\n");
	printf("This means that if you were to solve the linear system Ax=b for vector x,\n"
	       "in the worst case you would lose %.1f digits (%.1f bits) of precision.\n",
	       mc.log2_cond_number / log2(10.0),
	       mc.log2_cond_number);
	printf("The condition number is how much errors in vector b would be amplified\n"
	       "when solving x even with infinite computational precision.\n");
	printf("\n");
	printf("Compare this to the precision of vectors b and x:\n");
	for (i = 0; i < (int)ARRAY_LENGTH(precisions); i++) {
		const struct prec_desc *p = &precisions[i];
		double bits = p->bits_significand - mc.log2_cond_number;
		double digits = bits / log2(10.0);
		int rank = fbf_get_rank(&mc.sigma, p->prec);

		printf("\n");
		printf("- %s precision floating point has %.1f digits (%.0f bits) of precision,\n",
		       p->name, p->bits_significand / log2(10.0),
		       p->bits_significand);

		if (digits > 0.9) {
			printf("  leaving your result with %.1f correct digits (%.0f correct bits).\n",
			       digits, floor(bits));
		} else {
			printf("  leaving your result with no correct digits.\n");
		}

		if (rank < 4) {
			printf("  %s precision, matrix A has rank %d which means that the solution space\n"
			       "  for x has %d dimension%s and therefore has many solutions.\n",
			       p->name, rank, 4 - rank, (4 - rank > 1) ? "s" : "");
		} else {
			printf("  %s precision, matrix A has full rank which means the solution x is\n"
			       "  unique.\n",
			       p->name);
		}
	}
	printf("\n");
	printf("NOTE! The above gives you only an upper limit on errors.\n"
	       "If the upper limit is low, you can be confident of your computations. But,\n"
	       "if the upper limit is high, it does not necessarily imply that your\n"
	       "computations will be doomed.\n");

	return 0;
}
