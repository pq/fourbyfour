/*
 * SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
 * SPDX-FileCopyrightText: 2022 Collabora, Ltd.
 *
 * SPDX-License-Identifier: MIT
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <getopt.h>

#include <fourbyfour.h>

static void
print_matrix(const struct fbf_matrix *mat)
{
	const char *sep = "";
	int row, col;

	for (row = 0; row < 4; row++) {
		for (col = 0; col < 4; col++) {
			printf("%s%.20e", sep,
			       fbf_matrix_elem_get(mat, row, col));
			sep = " ";
		}
	}
	printf("\n");
}

static bool
str2double(double *d_out, const char *str)
{
	double d;
	char *end;
	const char *errstr;
	int len = strlen(str);

	errno = 0;
	d = strtod(str, &end);
	if (errno == 0 && str + len == end) {
		*d_out = d;
		return true;
	}

	errstr = strerror(errno);
	if (str + len != end)
		errstr = "unexpected trailing characters";

	fprintf(stderr,
		"Error: could not interpret '%s' as a number: %s\n",
		str, errstr);
	return false;
}

static bool
str2long(long *l_out, const char *str)
{
	long l;
	char *end;
	const char *errstr;
	int len = strlen(str);

	errno = 0;
	l = strtol(str, &end, 10);
	if (errno == 0 && str + len == end) {
		*l_out = l;
		return true;
	}

	errstr = strerror(errno);
	if (str + len != end)
		errstr = "unexpected trailing characters";

	fprintf(stderr,
		"Error: could not interpret '%s' as an integer: %s\n",
		str, errstr);
	return false;
}

static void
print_help(const char *prog)
{
	printf(
		"Generate a pseudo-random 4x4 matrix with given parameters\n"
		"\n"
		"Usage: %s [options]\n"
		"Options may include:\n"
		"  -b, --bits B     Set matrix 2-norm condition number to 2**B\n"
		"  -c, --cond C     Set matrix 2-norm condition number to C.\n"
		"  -d, --det D      Set matrix absolute determinant value to D.\n"
		"  -h, --help       This help text.\n"
		"  -n, --count N    Number of matrices to generate.\n"
		"\n"
		"The generated matrices are printed one per line. Row-major vs. column-major\n"
		"ordering is insignificant, it doesn't affect the matrix properties.\n"
		"If none of B, C nor D is given, the elements will be in range [-1.0, 1.0].\n",
		prog);
}

struct gen_options {
	const char *prog;
	bool help;
	long count;

	uint32_t flags;
	double cond;
	double absdet;
};

static bool
parse_options(struct gen_options *opts, int argc, char *argv[])
{
	static const struct option long_opts[] = {
		{ "help",       no_argument,       NULL, 'h' },
		{ "count",      required_argument, NULL, 'n' },
		{ "cond",       required_argument, NULL, 'c' },
		{ "det",        required_argument, NULL, 'd' },
		{ "bits",       required_argument, NULL, 'b' },
		{ 0 }
	};
	static const char short_opts[] = "hn:c:d:b:";
	bool ret = true;
	long tmp;
	int c;

	opts->prog = argv[0];

	while (1) {
		c = getopt_long(argc, argv, short_opts, long_opts, NULL);
		if (c == -1)
			break;

		switch (c) {
		case 'h':
			opts->help = true;
			break;
		case 'n':
			if (!str2long(&opts->count, optarg))
				ret = false;
			break;
		case 'b':
			opts->flags |= FBF_MATRIX_RAND_FLAG_COND;
			if (str2long(&tmp, optarg))
				opts->cond = exp2(tmp);
			else
				ret = false;
			break;
		case 'c':
			opts->flags |= FBF_MATRIX_RAND_FLAG_COND;
			if (!str2double(&opts->cond, optarg))
				ret = false;
			break;
		case 'd':
			opts->flags |= FBF_MATRIX_RAND_FLAG_ABSDET;
			if (!str2double(&opts->absdet, optarg))
				ret = false;
			break;
		case '?':
			ret = false;
			break;
		default:
			fprintf(stderr, "Internal error in %s: char %d\n",
				__func__, c);
			ret = false;
			break;
		}
	}

	if (optind < argc) {
		fprintf(stderr, "Error: extra arguments given:");
		while (optind < argc)
			fprintf(stderr, " %s", argv[optind++]);
		fprintf(stderr, "\n");
		ret = false;
	}

	if (opts->count < 0) {
		fprintf(stderr, "Error: count is negative: %ld\n",
			opts->count);
		ret = false;
	}

	if (opts->flags & FBF_MATRIX_RAND_FLAG_COND && !(opts->cond >= 1.0)) {
		fprintf(stderr, "Error: condition number must be >= 1.0, but it is %f\n",
			opts->cond);
		ret = false;
	}

	if (opts->flags & FBF_MATRIX_RAND_FLAG_ABSDET && !(opts->absdet >= 0.0)) {
		fprintf(stderr, "Error: abs det must be non-negative, but it is %f\n",
			opts->cond);
		ret = false;
	}

	if (opts->flags & FBF_MATRIX_RAND_FLAG_COND &&
	    opts->flags & FBF_MATRIX_RAND_FLAG_ABSDET) {
		if ((isinf(opts->cond) == 1 && opts->absdet != 0.0) ||
		    (opts->absdet == 0.0 && isinf(opts->cond) != 1)) {
			fprintf(stderr, "Error: defined conflicting abs det and condition number.\n");
			ret = false;
		}
	}

	return ret;
}

int
main(int argc, char *argv[])
{
	struct gen_options opts = { .count = 1, };
	long i;
	
	if (!parse_options(&opts, argc, argv)) {
		print_help(opts.prog);
		return 1;
	}
	
	if (opts.help) {
		print_help(opts.prog);
		return 0;
	}

	for (i = 0; i < opts.count; i++) {
		struct fbf_matrix mat;
		
		fbf_matrix_rand(&mat, opts.flags,
				opts.cond, opts.absdet);
		print_matrix(&mat);
	}

	return 0;
}
