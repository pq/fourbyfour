/*
 * SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <fourbyfour.h>

/* weston */
#include "matrix.h"

#include <graphene.h>

union fbfb_vector {
	struct fbf_vector gold;
	struct weston_vector weston;
	graphene_vec4_t graphene;
};

union fbfb_matrix {
	struct fbf_matrix gold;
	struct weston_matrix weston;
	graphene_matrix_t graphene;
};

union fbfb_decomposition {
	struct {
		double LU[16];
		unsigned perm[4];
	} weston;
};

enum fbfb_inversion_result {
	FBFB_INVERSION_RESULT_OK,
	FBFB_INVERSION_RESULT_NOT_INVERTIBLE,
};

/** Interface to different 4x4 matrix inversion implementations */
struct fbfb_interface {
	const char *name;
	const char *desc;

	enum fbf_precision precision;

	/** Convert implementation specific matrix to fbf_matrix */
	void
	(*matrix_to_fbf)(struct fbf_matrix *out, const union fbfb_matrix *in);

	/** Convert fbf_matrix to implementation specific matrix */
	void
	(*matrix_from_fbf)(union fbfb_matrix *out, const struct fbf_matrix *in);

	/** Convert implementation specific vector to fbf_vector */
	void
	(*vector_to_fbf)(struct fbf_vector *out, const union fbfb_vector *in);

	/** Convert fbf_vector to implementation specific vector */
	void
	(*vector_from_fbf)(union fbfb_vector *out, const struct fbf_vector *in);

	/** Compute inverse matrix explicitly */
	enum fbfb_inversion_result
	(*invert)(union fbfb_matrix *inv, const union fbfb_matrix *mat);

	/** Compute y = Ax */
	void
	(*transform)(union fbfb_vector *y,
		     const union fbfb_matrix *A,
		     const union fbfb_vector *x);

	/** Compute a custom inversion object */
	enum fbfb_inversion_result
	(*decompose)(union fbfb_decomposition *A_dec,
		     const union fbfb_matrix *mat);

	/** Solve Ax=b for x given b */
	void
	(*solve)(union fbfb_vector *x,
		 const union fbfb_decomposition *A_dec,
		 const union fbfb_vector *b);
};

const struct fbfb_interface *
fbfb_interface_get_gold(void);

const struct fbfb_interface *
fbfb_interface_get_graphene(void);

const struct fbfb_interface *
fbfb_interface_get_noop(void);

const struct fbfb_interface *
fbfb_interface_get_weston(void);

enum fbfb_inverse_transform {
	/** Use a custom inverse transform solver */
	FBFB_INVERSE_TRANSFORM_SOLVER,

	/** Explicitly use the inverse matrix */
	FBFB_INVERSE_TRANSFORM_EXPLICIT,
};

struct fbfb_options {
	const char *prog;
	int (*operation)(const struct fbfb_options *opts);
	const struct fbfb_interface *impl;
	const char *data_out_file;
	int verbose_level;
	enum fbfb_inverse_transform invtform;
	bool quick_test;
};

int
fbfb_operation_precision(const struct fbfb_options *opts);

int
fbfb_operation_speed(const struct fbfb_options *opts);
