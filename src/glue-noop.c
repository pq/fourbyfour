/*
 * SPDX-FileCopyrightText: 2020 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <fourbyfour.h>

#include "fourbyfour-bench.h"

static void
fbfb_noop_matrix_to_fbf(struct fbf_matrix *out, const union fbfb_matrix *in)
{
	*out = in->gold;
}

static void
fbfb_noop_matrix_from_fbf(union fbfb_matrix *out, const struct fbf_matrix *in)
{
	out->gold = *in;
}

static void
fbfb_noop_vector_to_fbf(struct fbf_vector *out,
			const union fbfb_vector *in)
{
	*out = in->gold;
}

static void
fbfb_noop_vector_from_fbf(union fbfb_vector *out, const struct fbf_vector *in)
{
	out->gold = *in;
}

static enum fbfb_inversion_result
fbfb_noop_invert(union fbfb_matrix *inv, const union fbfb_matrix *mat)
{
	(void)inv;
	(void)mat;
	return FBFB_INVERSION_RESULT_NOT_INVERTIBLE;
}

static void
fbfb_noop_transform(union fbfb_vector *y,
		    const union fbfb_matrix *A,
		    const union fbfb_vector *x)
{
	(void)y;
	(void)A;
	(void)x;
}

static enum fbfb_inversion_result
fbfb_noop_decompose(union fbfb_decomposition *A_dec,
		    const union fbfb_matrix *mat)
{
	(void)A_dec;
	(void)mat;
	return FBFB_INVERSION_RESULT_NOT_INVERTIBLE;
}

static void
fbfb_noop_solve(union fbfb_vector *x,
		const union fbfb_decomposition *A_dec,
		const union fbfb_vector *b)
{
	(void)x;
	(void)A_dec;
	(void)b;
}

static const struct fbfb_interface implementation_noop = {
	.name = "no-op",
	.desc = "used to measure overhead in speed testing",
	.precision = FBF_PRECISION_DOUBLE,
	.matrix_to_fbf = fbfb_noop_matrix_to_fbf,
	.matrix_from_fbf = fbfb_noop_matrix_from_fbf,
	.vector_to_fbf = fbfb_noop_vector_to_fbf,
	.vector_from_fbf = fbfb_noop_vector_from_fbf,
	.invert = fbfb_noop_invert,
	.transform = fbfb_noop_transform,
	.decompose = fbfb_noop_decompose,
	.solve = fbfb_noop_solve,
};

const struct fbfb_interface *
fbfb_interface_get_noop(void)
{
	return &implementation_noop;
}
