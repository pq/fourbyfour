/*
 * SPDX-FileCopyrightText: 2020 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <fourbyfour.h>

#include "fourbyfour-bench.h"
#include "matrix.h"

static void
fbfb_weston_matrix_to_fbf(struct fbf_matrix *out, const union fbfb_matrix *in)
{
	const struct weston_matrix *mat = &in->weston;
	int i;

	for (i = 0; i < 4 * 4; i++)
		out->v[i] = mat->d[i];
}

static void
fbfb_weston_matrix_from_fbf(union fbfb_matrix *out, const struct fbf_matrix *in)
{
	struct weston_matrix *mat = &out->weston;
	int i;

	for (i = 0; i < 4 * 4; i++)
		mat->d[i] = in->v[i];
	mat->type = WESTON_MATRIX_TRANSFORM_OTHER;
}

static void
fbfb_weston_vector_to_fbf(struct fbf_vector *out,
			  const union fbfb_vector *in)
{
	const struct weston_vector *vec = &in->weston;
	int i;

	for (i = 0; i < 4; i++)
		out->v[i] = vec->f[i];
}

static void
fbfb_weston_vector_from_fbf(union fbfb_vector *out,
			    const struct fbf_vector *in)
{
	struct weston_vector *vec = &out->weston;
	int i;

	for (i = 0; i < 4; i++)
		vec->f[i] = in->v[i];
}

static enum fbfb_inversion_result
fbfb_weston_matrix_invert(union fbfb_matrix *inv,
			  const union fbfb_matrix *mat)
{
	if (weston_matrix_invert(&inv->weston, &mat->weston) < 0)
		return FBFB_INVERSION_RESULT_NOT_INVERTIBLE;
	else
		return FBFB_INVERSION_RESULT_OK;
}


static void
fbfb_weston_vector_transform(union fbfb_vector *y,
			     const union fbfb_matrix *A,
			     const union fbfb_vector *x)
{
	y->weston = x->weston;
	/* cast away const, it's an oversight in weston-matrix API */
	weston_matrix_transform((struct weston_matrix *)&A->weston, &y->weston);
}

static enum fbfb_inversion_result
fbfb_weston_matrix_decompose(union fbfb_decomposition *A_dec,
			     const union fbfb_matrix *mat)
{
	if (matrix_invert(A_dec->weston.LU, A_dec->weston.perm,
			  &mat->weston) < 0)
		return FBFB_INVERSION_RESULT_NOT_INVERTIBLE;
	else
		return FBFB_INVERSION_RESULT_OK;
}

static void
fbfb_weston_matrix_solve(union fbfb_vector *x,
			 const union fbfb_decomposition *A_dec,
			 const union fbfb_vector *b)
{
	x->weston = b->weston;
	inverse_transform(A_dec->weston.LU, A_dec->weston.perm, x->weston.f);
}

static const struct fbfb_interface implementation_weston = {
	.name = "weston-matrix",
	.desc = "the matrix implementation from Weston",
	.precision = FBF_PRECISION_SINGLE,
	.matrix_to_fbf = fbfb_weston_matrix_to_fbf,
	.matrix_from_fbf = fbfb_weston_matrix_from_fbf,
	.vector_to_fbf = fbfb_weston_vector_to_fbf,
	.vector_from_fbf = fbfb_weston_vector_from_fbf,
	.invert = fbfb_weston_matrix_invert,
	.transform = fbfb_weston_vector_transform,
	.decompose = fbfb_weston_matrix_decompose,
	.solve = fbfb_weston_matrix_solve,
};

const struct fbfb_interface *
fbfb_interface_get_weston(void)
{
	return &implementation_weston;
}
