/*
 * SPDX-FileCopyrightText: 2020 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <fourbyfour.h>

static void
test_epsilon(void)
{
	double d = nextafter(1.0, 2.0);
	float f = nextafterf(1.0f, 2.0f);

	fbf_assert_eq_double_releps(FBF_EPSILON_DOUBLE, d - 1.0, 1e-16);
	fbf_assert_eq_double_releps(FBF_EPSILON_SINGLE, f - 1.0f, 1e-7);

	printf("%s OK\n", __func__);
}

static void
test_matrix_layout(void)
{
	struct fbf_matrix M;
	int row, col;

	/*
	 * This should be the mathematical layout, regardless of column- or
	 * row-major.
	 */
	M = FBF_MATRIX_INIT_VISUAL(
		1, 5,  9, 13,
		2, 6, 10, 14,
		3, 7, 11, 15,
		4, 8, 12, 16
	);

	for (row = 0; row < 4; row++) {
		for (col = 0; col < 4; col++)
			fbf_assert_eq_double(fbf_matrix_elem_get(&M, row, col),
					     col * 4 + row + 1);
	}

	printf("%s matrix is:\n", __func__);
	fbf_matrix_print(stdout, &M);

	printf("%s OK\n", __func__);
}

static void
test_matrix_column_getset(void)
{
	struct fbf_matrix M;
	struct fbf_matrix M_ref;
	struct fbf_vector vec = { { 101, 102, 103, 104 } };
	int i;

	M = FBF_MATRIX_INIT_VISUAL(
		1, 5,  9, 13,
		2, 6, 10, 14,
		3, 7, 11, 15,
		4, 8, 12, 16
	);

	fbf_matrix_column_set(&M, &vec, 1);
	fbf_matrix_column_get(&vec, &M, 0);
	fbf_matrix_column_set(&M, &vec, 3);


	M_ref = FBF_MATRIX_INIT_VISUAL(
		1, 101,  9, 1,
		2, 102, 10, 2,
		3, 103, 11, 3,
		4, 104, 12, 4
	);

	for (i = 0; i < 4 * 4; i++)
		fbf_assert_eq_double(M.v[i], M_ref.v[i]);

	printf("%s OK\n", __func__);
}

static void
test_matrix_identity(void)
{
	struct fbf_matrix M;
	struct fbf_matrix I;
	int i;

	M = FBF_MATRIX_INIT_VISUAL(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	);

	fbf_matrix_identity(&I);

	for (i = 0; i < 4 * 4; i++)
		fbf_assert_eq_double(M.v[i], I.v[i]);

	printf("%s matrix is:\n", __func__);
	fbf_matrix_print(stdout, &M);

	printf("%s OK\n", __func__);
}

static void
test_frobenius_norm(void)
{
	struct fbf_matrix M;
	double sum;
	double fnorm;
	int i;

	M = FBF_MATRIX_INIT_VISUAL(
		1.0,     0.7,   0.0,      -5.1,
		0.0089, -1.0,   2.1,       3.3333,
		0,      -100,   7.789111, -0.0,
		45e-5,   0.02, -0.02,      9
	);

	sum = 0.0;
	for (i = 0; i < 4 * 4; i++)
		sum += M.v[i] * M.v[i];

	fnorm = fbf_matrix_norm_F(&M);

	printf("%s matrix is:\n", __func__);
	fbf_matrix_print(stdout, &M);
	printf("Frobenius norm is %g\n", fnorm);

	fbf_assert_eq_double_releps(fnorm, sqrt(sum), 1e-9);

	printf("%s OK\n", __func__);
}

static void
test_infinity_norm(void)
{
	struct fbf_matrix M;
	double ref_norm = -1.0;
	double norm;
	double sum;
	int i, j;

	M = FBF_MATRIX_INIT_VISUAL(
		1.0,     0.7,   0.0,      -5.1,
		0.0089, -1.0,   2.1,       3.3333,
		0,      -100,   7.789111, -0.0,
		45e-5,   0.02, -0.02,      9
	);

	for (i = 0; i < 4; i++) {
		sum = 0.0;
		for (j = 0; j < 4; j++)
			sum += fabs(fbf_matrix_elem_get(&M, i, j));

		if (ref_norm < sum)
			ref_norm = sum;
	}

	norm = fbf_matrix_norm_inf(&M);

	printf("%s matrix is:\n", __func__);
	fbf_matrix_print(stdout, &M);
	printf("Infinity norm is %g\n", norm);

	fbf_assert_eq_double_releps(norm, ref_norm, 1e-9);

	printf("%s OK\n", __func__);
}

static void
test_matrix_diag_matrix_mul_trivial(void)
{
	struct fbf_matrix A;
	struct fbf_matrix B;
	struct fbf_vector d = {{ 2.0, 3.0, 17.0, -5.7 }};
	struct fbf_matrix R;
	struct fbf_matrix ref;

	fbf_matrix_identity(&A);
	fbf_matrix_identity(&B);

	fbf_matrix_diag_matrix_mul(&R, &A, &d, &B);

	ref = FBF_MATRIX_INIT_VISUAL(
		d.v[0], 0, 0, 0,
		0, d.v[1], 0, 0,
		0, 0, d.v[2], 0,
		0, 0, 0, d.v[3]
	);

	fbf_assert_eq_matrix_releps_inf(&R, &ref, 1e-9);

	printf("%s OK\n", __func__);
}

static void
test_matrix_diag_matrix_mul(void)
{
	struct fbf_matrix A;
	struct fbf_matrix B;
	struct fbf_vector d = {{ 2.0, 3.0, -2.0, 4.0 }};
	struct fbf_matrix R;
	struct fbf_matrix ref;

	A = FBF_MATRIX_INIT_VISUAL(
		 2,  3,  4,  5,
		 6,  7,  8,  9,
		-1, -2, -3, -4,
		-5, -6, -7, -8
	);

	B = FBF_MATRIX_INIT_VISUAL(
		 0.1,  0.2,  0.3,  0.4,
		-0.5, -0.6, -0.7, -0.8,
		 0.5,  0.6,  0.7,  0.8,
		-0.11, 0.45, 0.01, 0.001
	);

	fbf_matrix_diag_matrix_mul(&R, &A, &d, &B);

	/* From Octave */
	ref = FBF_MATRIX_INIT_VISUAL(
		-10.30000,   -0.40000,  -10.50000,  -11.98000,
		-21.26000,   -3.60000,  -21.94000,  -24.76400,
		  7.56000,   -0.40000,    7.64000,    8.78400,
		 18.52000,    2.80000,   19.08000,   21.56800
	);

	fbf_assert_eq_matrix_releps_inf(&R, &ref, 1e-9);

	printf("%s OK\n", __func__);
}

static void
test_svd(void)
{
	struct fbf_matrix A;
	struct fbf_matrix U;
	struct fbf_vector S;
	struct fbf_matrix VT;
	struct fbf_matrix U_ref;
	struct fbf_vector S_ref;
	struct fbf_matrix VT_ref;
	struct fbf_matrix A2;
	int i;

	/* Arbitrary matrix */
	A = FBF_MATRIX_INIT_VISUAL(
		 2,  3,   4,  5,
		 6,  7,  15,  9,
		-1, -4,  -3, -4,
		-5, -6,  -7, -8
	);

	fbf_matrix_SVD(&U, &S, &VT, &A);

	/* Reference results from Octave svd(), format long */
	U_ref = FBF_MATRIX_INIT_VISUAL(
		-2.840718486856285e-01,   3.199465468852253e-01,  -6.343937552904966e-03,   9.038236257018704e-01,
		-7.741509655773451e-01,  -6.218777663125673e-01,   1.160098597721595e-01,  -2.236154505842591e-02,
		 2.401358246825365e-01,  -4.606755844222467e-01,  -8.221474508350528e-01,   2.327796398027199e-01,
		 5.121799029482036e-01,  -5.465164744264659e-01,   5.572925945833062e-01,   3.583524162316221e-01
	);

	S_ref = (struct fbf_vector){{
		2.533869871109101e+01,
		3.998239732330517e+00,
		1.512790351074186e+00,
		8.221265261899565e-01
	}};

	VT_ref = FBF_MATRIX_INIT_VISUAL(
		-3.162784688209359e-01,  -4.066860393666198e-01,  -6.730502962104730e-01,  -5.306389491128327e-01,
		 2.548232704151467e-02,   4.323143624276803e-01,  -7.104972245473086e-01,   5.546608305603247e-01,
		-8.467427344739284e-01,   4.877552537685330e-01,   1.851983895811151e-01,  -1.040341796400103e-01,
		-4.270191129029800e-01,  -6.401605812952208e-01,   8.887378228773563e-02,   6.324164436454744e-01
	);

	fbf_assert_eq_matrix_releps_inf(&U, &U_ref, 1e-9);
	fbf_assert_eq_matrix_releps_inf(&VT, &VT_ref, 1e-9);
	for (i = 0; i < 4; i++)
		fbf_assert_eq_double_releps(S.v[i], S_ref.v[i], 1e-9);

	fbf_matrix_diag_matrix_mul(&A2, &U, &S, &VT);

	fbf_assert_eq_matrix_releps_inf(&A2, &A, 1e-9);

	printf("%s OK\n", __func__);
}

static void
test_matrix_cond_analyze(void)
{
	struct fbf_matrix M;
	struct fbf_matrix_conditions mc;

	M = FBF_MATRIX_INIT_VISUAL(
		 2,  3,   4,  5,
		 6,  7,  15,  9,
		-1, -4,  -3, -4,
		-5, -6,  -7, -8
	);
	fbf_matrix_cond_analyse(&mc, &M, FBF_PRECISION_DOUBLE);
	printf("A non-singular matrix:\n");
	fbf_matrix_print(stdout, &M);
	fbf_matrix_conditions_print(stdout, &mc);
	fbf_assert_eq_double(mc.rank, 4);

	M = FBF_MATRIX_INIT_VISUAL(
		 2,  3,   4,  5,
		 6,  7,  15,  9,
		-1, -4,  -3, -4,
		 2,  3,   4,  5
	);
	fbf_matrix_cond_analyse(&mc, &M, FBF_PRECISION_DOUBLE);
	printf("A singular matrix:\n");
	fbf_matrix_print(stdout, &M);
	fbf_matrix_conditions_print(stdout, &mc);
	fbf_assert_eq_double(mc.rank, 3);

	printf("%s OK\n", __func__);
}

int
main(void)
{
	test_epsilon();
	test_matrix_layout();
	test_matrix_column_getset();
	test_matrix_identity();
	test_frobenius_norm();
	test_infinity_norm();
	test_matrix_diag_matrix_mul_trivial();
	test_matrix_diag_matrix_mul();
	test_svd();
	test_matrix_cond_analyze();

	return 0;
}
