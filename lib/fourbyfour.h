/*
 * SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

#if !defined(FBF_STATIC_LIB) && defined(__GNUC__) && __GNUC__ >= 4
#define FBF_EXPORT __attribute__ ((visibility("default")))
#else
#define FBF_EXPORT
#endif

#ifndef ARRAY_LENGTH
#define ARRAY_LENGTH(a) (sizeof (a) / sizeof (a)[0])
#endif

#include <stdio.h>
#include <stdint.h>

/** The difference between 1.0 and the smallest value greater than 1.0,
 * in single precision floating point.
 */
#define FBF_EPSILON_SINGLE 1.1920929e-07

/** The difference between 1.0 and the smallest value greater than 1.0,
 * in double precision floating point.
 */
#define FBF_EPSILON_DOUBLE 2.220446049250313e-16

/** A column 4-vector, double precision */
struct fbf_vector {
	double v[4];
};

/** A 4x4 matrix in column-major order, double precision
 *
 * If matrix M in mathematical notation is defined as:
 *
 *     [  0  4  8 12 ]
 * M = [  1  5  9 13 ]
 *     [  2  6 10 14 ]
 *     [  3  7 11 15 ]
 *
 * then fbf_matrix.v = { 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 }.
 *
 * See: https://www.mathworks.com/help/coder/ug/what-are-column-major-and-row-major-representation-1.html
 */
struct fbf_matrix {
	double v[16];
};

/** Inline intuitive initialization of a matrix
 *
 * You can type out the matrix in the mathematical notation, and
 * this macro takes care of the column-major re-ordering.
 */
#define FBF_MATRIX_INIT_VISUAL( \
	a11, a12, a13, a14, \
	a21, a22, a23, a24, \
	a31, a32, a33, a34, \
	a41, a42, a43, a44) \
	(struct fbf_matrix){{ \
	[ 0] = (a11), [ 4] = (a12), [ 8] = (a13), [12] = (a14), \
	[ 1] = (a21), [ 5] = (a22), [ 9] = (a23), [13] = (a24), \
	[ 2] = (a31), [ 6] = (a32), [10] = (a33), [14] = (a34), \
	[ 3] = (a41), [ 7] = (a42), [11] = (a43), [15] = (a44) \
	}}

/** Access element (row, column) of fbf_matrix
 *
 * Rows and columns are numbered starting from zero, hence 0 .. 3.
 */
static inline double *
fbf_matrix_elem(struct fbf_matrix *M, int row, int column)
{
	return &M->v[column * 4 + row];
}

/** Get element (row, column) of fbf_matrix
 *
 * Rows and columns are numbered starting from zero, hence 0 .. 3.
 */
static inline double
fbf_matrix_elem_get(const struct fbf_matrix *M, int row, int column)
{
	return M->v[column * 4 + row];
}

/** Copy a column from a matrix into a vector */
static inline void
fbf_matrix_column_get(struct fbf_vector *vec,
		      const struct fbf_matrix *M,
		      int column)
{
	int i;

	for (i = 0; i < 4; i++)
		vec->v[i] = fbf_matrix_elem_get(M, i, column);
}

/** Copy a vector into a matrix column */
static inline void
fbf_matrix_column_set(struct fbf_matrix *M,
		      const struct fbf_vector *vec,
		      int column)
{
	int i;

	for (i = 0; i < 4; i++)
		*fbf_matrix_elem(M, i, column) = vec->v[i];
}

/** Initialize matrix to all zeros. */
static inline void
fbf_matrix_zero(struct fbf_matrix *M)
{
	int i;

	for (i = 0; i < 4 * 4; i++)
		M->v[i] = 0.0;
}

/** Initialize matrix to identity. */
static inline void
fbf_matrix_identity(struct fbf_matrix *M)
{
	int i;

	for (i = 0; i < 4 * 4; i++)
		M->v[i] = (i % 5 == 0) ? 1.0 : 0.0;
}

struct fbf_matrix_conditions {
	/**
	 * Base-2 logarithm of a matrix condition number according to 2-norm.
	 * This is an estimate of how many bits of precision may be lost,
	 * when solving a linear system described by the matrix.
	 *
	 * https://mathworld.wolfram.com/ConditionNumber.html
	 *
	 * The 2-norm condition number is the ratio between the largest and
	 * smallest singular value.
	 */
	double log2_cond_number;

	/** Matrix rank, see fbf_get_rank() */
	int rank;

	/** Absolute value of determinant (product of singular values) */
	double abs_det;

	/** The singular values */
	struct fbf_vector sigma;
};

/* asserts.c */

void
fbf_assert_eq_double(double x, double ref);

void
fbf_assert_eq_double_releps(double x, double ref, double eps);

void
fbf_assert_eq_vector_releps(const struct fbf_vector *x,
			    const struct fbf_vector *ref, double eps);

void
fbf_assert_eq_matrix_releps_inf(const struct fbf_matrix *A,
				const struct fbf_matrix *A_ref,
				double eps);

/* blas-wrap.c */

void
fbf_matrix_matrix_mul(struct fbf_matrix *M,
		      const struct fbf_matrix *A,
		      const struct fbf_matrix *B);

void
fbf_matrix_diag_matrix_mul(struct fbf_matrix *M,
			   const struct fbf_matrix *A,
			   const struct fbf_vector *D,
			   const struct fbf_matrix *B);

void
fbf_matrix_diff(struct fbf_matrix *M,
		const struct fbf_matrix *A,
		const struct fbf_matrix *B);

/* generator.c */

enum fbf_matrix_rand_flags {
	/** Unconstrained random [-1.0, 1.0] matrix */
	FBF_MATRIX_RAND_FLAG_NONE        = 0,

	/** Restrict 2-norm condition number */
	FBF_MATRIX_RAND_FLAG_COND        = 0x1,

	/** Restrict determinant absolute value */
	FBF_MATRIX_RAND_FLAG_ABSDET      = 0x2,
};

void
fbf_matrix_rand(struct fbf_matrix *M,
		uint32_t flags,
		double cond,
		double abs_det);

double
fbf_rand_zero_one(void);

/* lapack-wrap.c */

double
fbf_matrix_norm_F(const struct fbf_matrix *M);

double
fbf_matrix_norm_inf(const struct fbf_matrix *M);

void
fbf_matrix_SVD(struct fbf_matrix *U_out,
	       struct fbf_vector *S_out,
	       struct fbf_matrix *VT_out,
	       const struct fbf_matrix *A_in);

/* util.c */

enum fbf_precision {
	FBF_PRECISION_SINGLE,
	FBF_PRECISION_DOUBLE,
};

int
fbf_get_rank(const struct fbf_vector *sigma, enum fbf_precision p);

void
fbf_matrix_cond_analyse(struct fbf_matrix_conditions *mc,
			const struct fbf_matrix *M,
			enum fbf_precision p);

void
fbf_matrix_conditions_print(FILE *fp, const struct fbf_matrix_conditions *mc);

void
fbf_matrix_print(FILE *fp, const struct fbf_matrix *M);

double
fbf_matrix_rel_error_F(const struct fbf_matrix *A,
		       const struct fbf_matrix *B);

double
fbf_matrix_rel_error_inf(const struct fbf_matrix *A,
			 const struct fbf_matrix *B);

/** For printing a value in metric prefix units */
struct fbf_value_units {
	double value;
	const char *units;
};

void
fbf_metric_prefix(struct fbf_value_units *uvu, double value);
