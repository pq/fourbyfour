/*
 * SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <stdio.h>
#include <math.h>

#include "fourbyfour.h"

/** Determine human readable metrix prefix for a value */
FBF_EXPORT void
fbf_metric_prefix(struct fbf_value_units *uvu, double value)
{
	const char *pfx[] = {
		"a", "f", "p", "n", "µ", "m", "",
		"k", "M", "G", "T", "P", "E"
	};
	const int unity_index = 6;
	int ex;

	if (value > 0.0)
		ex = floor(log10(value) / 3);
	else
		ex = 0;

	ex += unity_index;
	if (ex < 0)
		ex = 0;
	if (ex >= (int)ARRAY_LENGTH(pfx))
		ex = ARRAY_LENGTH(pfx) - 1;

	value *= exp10(-(ex - unity_index) * 3);

	uvu->value = value;
	uvu->units = pfx[ex];
}

/** Print matrix to file */
FBF_EXPORT void
fbf_matrix_print(FILE *fp, const struct fbf_matrix *M)
{
	int row, col;

	for (row = 0; row < 4; row++) {
		for (col = 0; col < 4; col++) {
			fprintf(fp, "%s%12.8g", col == 0 ? "\t" : " ",
				fbf_matrix_elem_get(M, row, col));
		}
		fprintf(fp, "\n");
	}
}

/** Relative error between matrices according to Frobenius norm
 *
 * Computes fbf_matrix_norm_F(A - B) / fbf_matrix_norm_F(B).
 *
 * See: http://www.netlib.org/lapack/lug/node75.html
 * LAPACK Users' Guide: Accuracy and Stability: How to Measure Errors
 */
FBF_EXPORT double
fbf_matrix_rel_error_F(const struct fbf_matrix *A,
		       const struct fbf_matrix *B)
{
	struct fbf_matrix diff;

	fbf_matrix_diff(&diff, A, B);
	return fbf_matrix_norm_F(&diff) / fbf_matrix_norm_F(B);
}

/** Relative error between matrices according to infinity norm
 *
 * Computes fbf_matrix_norm_inf(A - B) / fbf_matrix_norm_inf(B).
 *
 * See: http://www.netlib.org/lapack/lug/node75.html
 * LAPACK Users' Guide: Accuracy and Stability: How to Measure Errors
 */
FBF_EXPORT double
fbf_matrix_rel_error_inf(const struct fbf_matrix *A,
			 const struct fbf_matrix *B)
{
	struct fbf_matrix diff;

	fbf_matrix_diff(&diff, A, B);
	return fbf_matrix_norm_inf(&diff) / fbf_matrix_norm_inf(B);
}

/** Estimate matrix rank from singular values
 *
 * This uses the method from rank() in Octave.
 */
FBF_EXPORT int
fbf_get_rank(const struct fbf_vector *sigma, enum fbf_precision p)
{
	double tol = 4 * sigma->v[0];
	int i;

	switch (p) {
	case FBF_PRECISION_SINGLE:
		tol *= FBF_EPSILON_SINGLE;
		break;
	case FBF_PRECISION_DOUBLE:
		tol *= FBF_EPSILON_DOUBLE;
		break;
	}

	for (i = 0; i < 4; i++) {
		if (sigma->v[i] <= tol)
			return i;
	}

	return 4;
}

/** Analyze matrix numerical conditions
 *
 * \param mc The returned information.
 * \param M The matrix to analyze.
 * \param p Precision for estimating matrix rank.
 *
 * Computes the values described in struct fbf_matrix_conditions from
 * the given matrix.
 */
FBF_EXPORT void
fbf_matrix_cond_analyse(struct fbf_matrix_conditions *mc,
			const struct fbf_matrix *M,
			enum fbf_precision p)
{
	int i;

	/* Find singular values */
	fbf_matrix_SVD(NULL, &mc->sigma, NULL, M);

	/*
	 * The 2-norm condition number is largest singular value divided by
	 * smallest singular value.
	 */
	mc->log2_cond_number = log2(mc->sigma.v[0]) - log2(mc->sigma.v[3]);

	mc->rank = fbf_get_rank(&mc->sigma, p);

	/* Absolute value of determinant is the product of singular values. */
	mc->abs_det = mc->sigma.v[0];
	for (i = 1; i < 4; i++)
		mc->abs_det *= mc->sigma.v[i];
}

FBF_EXPORT void
fbf_matrix_conditions_print(FILE *fp, const struct fbf_matrix_conditions *mc)
{
	fprintf(fp,
		"\tRank %d, log2(cond) = %.3f, |det| = %g,\n\tsingular values: %g %g %g %g.\n",
		mc->rank, mc->log2_cond_number, mc->abs_det,
		mc->sigma.v[0], mc->sigma.v[1], mc->sigma.v[2], mc->sigma.v[3]);
}
