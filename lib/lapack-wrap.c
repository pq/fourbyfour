/*
 * SPDX-FileCopyrightText: 2020 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <stdio.h>
#include <lapacke.h>

#include "fourbyfour.h"

/** Singular Value Decomposition of 4x4 matrix
 *
 * \param U_out Optional output parameter for matrix U.
 * \param S_out Mandatory output parameter for the diagonal.
 * \param VT_out Optional output parameter for the matrix V^T.
 * \param A_in The matrix to decompose.
 *
 * Computes double precision SVD using LAPACK: A = U * diag(S) * VT
 */
FBF_EXPORT void
fbf_matrix_SVD(struct fbf_matrix *U_out,
	       struct fbf_vector *S_out,
	       struct fbf_matrix *VT_out,
	       const struct fbf_matrix *A_in)
{
	struct fbf_matrix R = *A_in;
	double superb[3]; /* written only on error */
	lapack_int ret;

	ret = LAPACKE_dgesvd(LAPACK_COL_MAJOR,
			     U_out ? 'A' : 'N',
			     VT_out ? 'A' : 'N',
			     4, 4, R.v, 4,
			     S_out->v,
			     U_out ? U_out->v : NULL, 4,
			     VT_out ? VT_out->v : NULL, 4,
			     superb);

	if (ret != 0) {
		fprintf(stderr, "Computing SVD of the following matrix failed, error %lld:\n",
			(long long int)ret);
		fbf_matrix_print(stderr, A_in);
		abort();
	}
}

/** Frobenius norm of a matrix
 *
 * Square root of sum of squares.
 */
FBF_EXPORT double
fbf_matrix_norm_F(const struct fbf_matrix *M)
{
	return LAPACKE_dlange(LAPACK_COL_MAJOR, 'F', 4, 4, M->v, 4);
}

/** Infinity norm of a matrix
 *
 * Maximum row sum of absolute values.
 */
FBF_EXPORT double
fbf_matrix_norm_inf(const struct fbf_matrix *M)
{
	return LAPACKE_dlange(LAPACK_COL_MAJOR, 'i', 4, 4, M->v, 4);
}
