/*
 * SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#include "fourbyfour.h"

/** A random number in [0.0, 1.0]
 */
FBF_EXPORT double
fbf_rand_zero_one(void)
{
	/*
	 * RAND_MAX seems to be 2^31 - 1 in glibc, but double
	 * significand precision is 53 bits. Maybe twice
	 * RAND_MAX worth of bits is enough.
	 */
	uint64_t r = random();
	r *= (uint64_t)RAND_MAX + 1;
	r += random();

	return (double)r / (((uint64_t)RAND_MAX + 2) * RAND_MAX);
}

/** Randomize matrix with values [-1.0 .. 1.0] */
static void
fbf_matrix_rand_uniform(struct fbf_matrix *M)
{
	int i;

	for (i = 0; i < 4 * 4; i++)
		M->v[i] = fbf_rand_zero_one() * 2.0 - 1.0;
}

/** Randomize matrix with condition number
 *
 * \param M Matrix to initialize.
 * \param flags Bit-wise or of enum fbf_matrix_rand_flags.
 * \param cond The desired 2-norm condition number >= 1.0 if
 * flags contains FBF_MATRIX_RAND_FLAG_COND, ignored otherwise.
 * \param abs_det The desired determinant absolute value if
 * flags contains FBF_MATRIX_RAND_FLAG_ABSDET, ignored otherwise.
 *
 * Produces a random matrix with elements in the range [-1.0, 1.0] and
 * then applies the desired restrictions, altering the element values.
 *
 * If the desired condition number is used and is Inf,
 * or the desired determinant absolute value is used and is zero,
 * the smallest singular value is set to zero exactly.
 *
 * If both desired condition number and determinant absolute value
 * are used and they conflict (cond == Inf and abs_det != 0, or
 * cond != Inf and abs_det == 0), the results are undefined.
 */
FBF_EXPORT void
fbf_matrix_rand(struct fbf_matrix *M,
		uint32_t flags,
		double cond,
		double abs_det)
{
	struct fbf_matrix R;
	struct fbf_matrix U;
	struct fbf_vector S;
	struct fbf_matrix VT;
	double tmp;
	double orig_abs_det;
	int i;

	/* Completely random matrix with elements [-1.0, 1.0] */
	fbf_matrix_rand_uniform(&R);

	if (flags == FBF_MATRIX_RAND_FLAG_NONE) {
		*M = R;
		return;
	}

	/* Singular Value Decomposition: R = U * S * V^T */
	fbf_matrix_SVD(&U, &S, &VT, &R);

	/*
	 * The 2-norm matrix Condition Number is the ratio between
	 * the largest and smallest singular value. Scale the singular
	 * values to reach the desired condition number.
	 *
	 * We do not have symmetric matrices, but this should work:
	 * https://www.mathworks.com/matlabcentral/answers/339173-creating-random-dense-symmetric-matrix-with-desired-condition-number
	 *
	 * A similar solution, but ignoring the original singular values:
	 * https://math.stackexchange.com/questions/198515/can-we-generate-random-singular-matrices-with-desired-condition-number-using-mat
	 *
	 * Here we use logarithmic scaling while preserving the largest
	 * singular value S.v[0]. In the result, S.v[0] / S.v[3] == cond.
	 */
	if (flags & FBF_MATRIX_RAND_FLAG_COND) {
		if (isinf(cond)) {
			S.v[3] = 0.0;
		} else {
			if (cond < 1.0) {
				fprintf(stderr,
					"Error: Desired condition number %f < 1.0.\n",
					cond);
				abort();
			}

			tmp = log(cond);
			for (i = 1; i < 4; i++) {
				S.v[i] = exp((double)i / 3.0 * -tmp) * S.v[0];
			}
		}
	}

	/*
	 * Absolute value of matrix determinant is the product of singular
	 * values. Adjust singular values without changing the condition
	 * number to produce the desired determinant.
	 */
	if (flags & FBF_MATRIX_RAND_FLAG_ABSDET) {
		if (abs_det == 0.0) {
			S.v[3] = 0.0;
		} else {
			orig_abs_det = S.v[0];
			for (i = 1; i < 4; i++)
				orig_abs_det *= S.v[i];

			tmp = pow(abs_det / orig_abs_det, 0.25);
			for (i = 0; i < 4; i++)
				S.v[i] *= tmp;
		}
	}

	/* Compose the final matrix: M = U * S * V^T */
	fbf_matrix_diag_matrix_mul(M, &U, &S, &VT);
}
